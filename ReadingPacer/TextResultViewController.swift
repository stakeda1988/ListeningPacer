import UIKit
import AVFoundation

class TextResultViewController: UIViewController, AVSpeechSynthesizerDelegate, NADViewDelegate {
    private var nadView: NADView!
    var adTimer:NSTimer!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pauseLabel: UIButton!
    var mainEnglishText:String = ""
    var synthesizer = AVSpeechSynthesizer()
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var showCountSecond: UILabel!
    var timer:NSTimer!
    var startCount = false
    var counter:Double = 0
    @IBAction func pauseLabel(sender: UIButton) {
        timer.invalidate()
        synthesizer.pauseSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        startButton.hidden = false
        pauseLabel.hidden = true
    }
    
    @IBAction func startSentences(sender: UIButton) {
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        startButton.hidden = true
        pauseLabel.hidden = false
        
        let string = englishLabel.text
        let utterance = AVSpeechUtterance(string: string!)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
        
        synthesizer.delegate = self
        
        if startCount == true {
            synthesizer.continueSpeaking()
        } else {
            synthesizer.speakUtterance(utterance)
        }
        
    }
    var mainRow:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        
        startCount = false
        pauseLabel.hidden = true
        englishLabel.hidden = false
        englishLabel.lineBreakMode = NSLineBreakMode.ByCharWrapping
        englishLabel.text = mainEnglishText
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance!) {
        let mutableAttributedString = NSMutableAttributedString(string: utterance.speechString)
        mutableAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: characterRange)
        englishLabel.attributedText = mutableAttributedString
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onAudioSessionRouteChange:", name: AVAudioSessionRouteChangeNotification, object: nil)
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        englishLabel.attributedText = NSAttributedString(string: utterance.speechString)
        startCount = false
        startButton.hidden = false
        pauseLabel.hidden = true
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didStartSpeechUtterance utterance: AVSpeechUtterance!)
    {
        startCount = true
    }
    
    func onUpdate(){
        counter += 0.1
    }
    
    @objc func onAudioSessionRouteChange(notification :NSNotification) {
        synthesizer.pauseSpeakingAtBoundary(AVSpeechBoundary.Immediate)
    }
    
    override func viewWillDisappear(animated: Bool) {
        if (timer != nil) {
            timer.invalidate()
        }
        synthesizer.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        startButton.hidden = false
        pauseLabel.hidden = true
        startCount = false
        counter = 0
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}